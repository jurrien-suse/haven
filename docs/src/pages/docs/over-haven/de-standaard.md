---
title: "De standaard"
path: "/docs/de-standaard"
---

Haven is een standaard voor cloud-agnostische infrastructuur en schrijft een specifieke configuratie van [Kubernetes](https://www.kubernetes.io) voor om het geschikt te maken voor gebruik binnen overheden.

Voorbeelden van specifieke configuratie zijn:

- Het cluster voldoet aan de [CNCF Conformance test](https://github.com/cncf/k8s-conformance#certified-kubernetes).
- Er worden centraal metrics verzameld waarmee de gezondheid van een cluster gemonitord kan worden en capaciteitsplanning uitgevoerd kan worden.
- De control plane van het cluster is High Available uitgevoerd.
- Worker nodes zijn uitgevoerd met SELinux, Grsecurity, AppArmor of LKRG.

Deze lijst is niet volledig. De Haven Compliancy Checker beschrijft namelijk alle onderdelen waaraan een cluster moet voldoen

## Haven Compliancy Checker

De [Haven Compliancy Checker](/docs/compliancy-checker) borgt de standaard door geautomatiseerd clusters te valideren. Dit is de essentie van Haven en de validatie staat op zichzelf.

Gebruik van een [Referentie Implementatie](https://gitlab.com/commonground/haven/haven/-/tree/master/reference) of [Addons](/docs/addons) is dus niet verplicht en heeft geen invloed op Haven Compliancy.

Er zijn verschillende implementaties van leveranciers en cloudproviders waarvan we hebben vastgesteld dat ze Haven Compliant zijn. Bekijk voor meer informatie de [aan de slag](/docs/aan-de-slag) pagina.

&ensp;

---

*Tijd om [aan de slag](/docs/aan-de-slag) te gaan!*
