import styled from 'styled-components/macro'
import { mediaQueries } from '@commonground/design-system'

export const StyledCategoryList = styled.div`
  display: flex;
  flex-wrap: wrap;

  ${mediaQueries.smUp`
    margin-left: -${(p) => p.theme.tokens.spacing04};
    margin-right: -${(p) => p.theme.tokens.spacing04};
  `}
`

export const StyledCategoryColumn = styled.div`
  ul {
    margin: 0;
    padding-left: 0;
    list-style: none;
  }

  h3 {
    margin: 0 0 0.75rem 0;
  }

  width: 100%;

  ${mediaQueries.smUp`
    width: 50%;

    padding-left: ${(p) => p.theme.tokens.spacing04};
    padding-right: ${(p) => p.theme.tokens.spacing04};
  `}
`

export const StyledCategory = styled.div`
  break-inside: avoid;
  margin-bottom: ${(p) => p.theme.tokens.spacing08};
`

export const StyledCheckList = styled.ul`
  border-top: 1px solid ${(p) => p.theme.tokens.colorPaletteGray300};
`

export const StyledCheck = styled.li`
  break-inside: avoid;
  padding-top: ${(p) => p.theme.tokens.spacing04};
  padding-bottom: ${(p) => p.theme.tokens.spacing04};
  padding-left: ${(p) => p.theme.tokens.spacing03};
  padding-right: ${(p) => p.theme.tokens.spacing03};
  border-bottom: 1px solid ${(p) => p.theme.tokens.colorPaletteGray300};
`

export const StyledRationale = styled.p`
  margin-top: ${(p) => p.theme.tokens.spacing04};
`
