import React, { Fragment } from 'react'
import { Link } from 'gatsby'

import { StyledSideNav } from './index.styles'
import menu from '../../pages/docs/menu.json'

const SideNav = () => (
  <StyledSideNav width={1/4}>
    {menu.map((category, i) => (
        <Fragment key={i}>
          <b>{category.title}</b>
          <ul>
            {category.children.map((child, j) => (
              <li key={j}><Link to={child.link} activeClassName="active">{child.title}</Link></li>
            ))}
          </ul>
        </Fragment>
    ))}

    <hr />
  </StyledSideNav>
)

export default SideNav
