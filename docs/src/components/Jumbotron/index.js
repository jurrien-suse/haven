import styled from 'styled-components/macro'

const Jumbotron = styled.div`
  ${(p) => p.isDark ? `
    color: white;
    background: rgb(58,58,58);
    background: url('/images/background-checks.svg'), linear-gradient(160deg, rgba(58,58,58,1) 0%, rgba(34,34,34,1) 100%);
    background-position-x: center;
  ` : null}

  text-align: ${(p) => p.textAlign ? p.textAlign : 'center'};

  padding-top: ${(p) => p.theme.tokens.spacing11};
  padding-bottom: ${(p) => p.theme.tokens.spacing11};
`

Jumbotron.LargeParagraph = styled.p`
  font-size: 1.5rem;
`

Jumbotron.Paragraph = styled.p`
  font-size: 18px;
`

Jumbotron.LargeHeading = styled.h1`
  font-size: 3rem;
`

export default Jumbotron
