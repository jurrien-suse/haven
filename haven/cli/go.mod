module gitlab.com/commonground/haven/haven/cli

go 1.13

require (
	github.com/AlecAivazis/survey/v2 v2.2.9
	github.com/Masterminds/semver/v3 v3.1.1
	github.com/googleapis/gnostic v0.3.1 // indirect
	github.com/gookit/color v1.3.8
	github.com/gorilla/mux v1.8.0
	github.com/jawher/mow.cli v1.2.0
	github.com/lunixbochs/vtclean v1.0.0
	github.com/markbates/pkger v0.17.1
	github.com/olekukonko/tablewriter v0.0.5
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
	helm.sh/helm/v3 v3.3.4
	k8s.io/api v0.18.14
	k8s.io/apiextensions-apiserver v0.18.14
	k8s.io/apimachinery v0.18.14
	k8s.io/cli-runtime v0.18.14
	k8s.io/client-go v0.18.14
	k8s.io/kubectl v0.18.14
	sigs.k8s.io/yaml v1.2.0
)

replace (
	github.com/Azure/go-autorest => github.com/Azure/go-autorest v13.3.2+incompatible
	github.com/docker/distribution => github.com/docker/distribution v0.0.0-20191216044856-a8371794149d
)
