// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package main

import (
	"os"

	"github.com/gookit/color"
	cli "github.com/jawher/mow.cli"
	"gitlab.com/commonground/haven/haven/cli/pkg/addons"
	"gitlab.com/commonground/haven/haven/cli/pkg/compliancy"
	"gitlab.com/commonground/haven/haven/cli/pkg/dashboard"
	"gitlab.com/commonground/haven/haven/cli/pkg/logging"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
)

var (
	version = "undefined"
)

func main() {
	app := cli.App("haven", "")

	logging.LogFile = app.StringOpt("l log-file", "", "Write logs to file (optional)")
	logging.OutputFormat = app.StringOpt("o output", "text", "Specify output format (text, json)")

	app.Before = func() {
		if "text" != *logging.OutputFormat && "json" != *logging.OutputFormat {
			logging.Fatal("Invalid output format: '%s'. Try 'text' or 'json'\n", logging.OutputFormat)
			return
		}

		logging.Info(color.Bold.Sprintf("Haven %s - Copyright © VNG Realisatie 2019-2021 - Licensed under the EUPL v1.2.\n", version))

		compliancy.Version = version
	}

	app.Command("check", "Runs compliancy checks", compliancy.CLIConfig)
	app.Command("addons", "Manage addons on a cluster", addons.CLIConfig)
	app.Command("dashboard", "Start the Haven dashboard", dashboard.CLIConfig)

	app.Run(os.Args)
}
