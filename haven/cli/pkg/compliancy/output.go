// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

import "time"

type CompliancyOutput struct {
	Version        string
	HavenCompliant bool
	StartTS        time.Time
	StopTS         time.Time
	Config         struct {
		SSH  bool
		CNCF bool
		CIS  bool
	}
	CompliancyChecks struct {
		Results []Check
		Summary struct {
			Total   int
			Unknown int
			Skipped int
			Failed  int
			Passed  int
		}
	}
	SuggestedChecks struct {
		Results []Check
	}
}

type RationaleOutput struct {
	Version          string
	CompliancyChecks []Check
	SuggestedChecks  []Check
}
