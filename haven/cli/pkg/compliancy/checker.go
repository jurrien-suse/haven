// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

import (
	"errors"
	"fmt"
	"regexp"
	"strings"

	"github.com/Masterminds/semver/v3"
	"github.com/gookit/color"
	"github.com/olekukonko/tablewriter"
	"gitlab.com/commonground/haven/haven/cli/pkg/logging"
	extensionsclient "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// Checker runs the compliancy checks and returns a list of results
type Checker struct {
	config *Config
}

type Platform int

const (
	PlatformUnknown Platform = iota
	PlatformEKS              // Amazon AWS EKS
	PlatformAKS              // Microsoft Azure AKS
	PlatformGKE              // Google Cloud Platform GKE
)

// NewChecker returns a new Checker
func NewChecker(kubeConfig *rest.Config, kubeLatest string, sshHost string, cncfConfigured bool, runCISChecks bool) (*Checker, error) {
	kubeClient, err := kubernetes.NewForConfig(kubeConfig)
	if err != nil {
		return nil, err
	}

	extensionsClient, err := extensionsclient.NewForConfig(kubeConfig)
	if err != nil {
		return nil, err
	}

	kubeServerSrc, err := kubeClient.Discovery().ServerVersion()
	if err != nil {
		return nil, err
	}

	kubeServer, err := semver.NewVersion(kubeServerSrc.String())
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Kubernetes server version: %s, Error: %s", kubeServerSrc.String(), err.Error()))
	}

	platform := PlatformUnknown

	awsMasterPattern := regexp.MustCompile(`https:\/\/.*.eks.amazonaws.com`)
	if awsMasterPattern.MatchString(kubeConfig.Host) {
		platform = PlatformEKS
	}

	azureMasterPattern := regexp.MustCompile(`https:\/\/.*.azmk8s.io`)
	if azureMasterPattern.MatchString(kubeConfig.Host) {
		platform = PlatformAKS
	}

	serverVersion, err := kubeClient.DiscoveryClient.ServerVersion()
	googleMasterVersionPattern := regexp.MustCompile(`gke`)
	if googleMasterVersionPattern.MatchString(serverVersion.String()) {
		platform = PlatformGKE
	}

	return &Checker{
		config: &Config{
			KubeConfig:       kubeConfig,
			KubeClient:       kubeClient,
			KubeServer:       kubeServer,
			KubeLatest:       kubeLatest,
			ExtensionsClient: extensionsClient,
			SSHHost:          sshHost,
			CNCFConfigured:   cncfConfigured,
			RunCISChecks:     runCISChecks,
			HostPlatform:     platform,
		},
	}, nil
}

// Run the checker and return a slice of results
func (c *Checker) Run() error {
	for i, _ := range checks.CompliancyChecks {
		if CheckInheritsEnv(checks.CompliancyChecks[i], *c.config) {
			checks.CompliancyChecks[i].Result = ResultYes
			continue
		}

		if c.config.SSHHost == "" && checks.CompliancyChecks[i].SSHRequired {
			checks.CompliancyChecks[i].Result = ResultSkipped
			continue
		}

		if !c.config.CNCFConfigured && checks.CompliancyChecks[i].Name == "cncf" {
			checks.CompliancyChecks[i].Result = ResultSkipped
			continue
		}

		compliancyResult, err := checks.CompliancyChecks[i].Exec(c.config)
		if err != nil {
			logging.Fatal("Compliancy Check '%s' interruption\n", checks.CompliancyChecks[i].Label)

			return err
		}

		checks.CompliancyChecks[i].Result = compliancyResult
	}

	for i, _ := range checks.SuggestedChecks {
		if !c.config.RunCISChecks && checks.SuggestedChecks[i].Name == "cis" {
			checks.SuggestedChecks[i].Result = ResultSkipped
			continue
		}

		suggestedResult, err := checks.SuggestedChecks[i].Exec(c.config)
		if err != nil {
			logging.Fatal("Suggested Check '%s' interruption\n", checks.SuggestedChecks[i].Label)

			return err
		}

		checks.SuggestedChecks[i].Result = suggestedResult
	}

	return nil
}

// PrintResults prints the table of checks with the corresponding results
func (c *Checker) PrintResults(checks []Check, compliancyChecks bool) {
	tableOut := &strings.Builder{}
	table := tablewriter.NewWriter(tableOut)

	table.SetHeader([]string{"Category", "Name", "Passed"})
	table.SetAutoWrapText(false)

	totalChecks := 0
	unknownChecks := 0
	skippedChecks := 0
	failedChecks := 0
	passedChecks := 0

	for _, check := range checks {
		totalChecks++

		switch check.Result {
		case ResultUnknown:
			unknownChecks++
		case ResultSkipped:
			skippedChecks++
		case ResultNo:
			failedChecks++
		case ResultYes:
			passedChecks++
		}

		table.Append([]string{string(check.Category), check.Label, check.Result.String()})
	}

	table.Render()

	prefix := "Compliancy checks results:\n"
	if compliancyChecks {
		compliancy := fmt.Sprintf("Results: %d out of %d checks passed, %d checks skipped, %d checks unknown.", passedChecks, totalChecks, skippedChecks, unknownChecks)

		if passedChecks == totalChecks {
			logging.Info(color.Green.Sprintf("%s This is a Haven Compliant cluster.\n", compliancy))

			output.HavenCompliant = true
		} else if failedChecks > 0 {
			logging.Error(color.Red.Sprintf("%s This is NOT a Haven Compliant cluster.\n", compliancy))
		} else {
			logging.Warning(color.Yellow.Sprintf("%s This COULD be a Haven Compliant cluster.\n", compliancy))
		}

		output.CompliancyChecks.Summary.Total = totalChecks
		output.CompliancyChecks.Summary.Passed = passedChecks
		output.CompliancyChecks.Summary.Failed = failedChecks
		output.CompliancyChecks.Summary.Skipped = skippedChecks
		output.CompliancyChecks.Summary.Unknown = unknownChecks
	} else {
		prefix = "Suggested checks results:\n"
	}

	logging.Info(fmt.Sprintf("%s\n%s\n", prefix, tableOut.String()))
}

// CheckInheritsEnv returns yes when a given check should implicitely pass given the underlying host platform.
// Explicit checking is preferred: only use whitelisting when there is no factual possibility to run a certain check,
// like not having SSH access to a managed control plane (there are no master instances).
func CheckInheritsEnv(check Check, config Config) bool {
	if check.Name == "nodehardening" {
		// AKS uses Ubuntu images having AppArmor enabled. This cannot be checked explicitely because there is no SSH access.
		if config.HostPlatform == PlatformAKS {
			return true
		}
	}

	if check.Name == "basicauth" {
		// AKS managed control plane is confirmed to have basic auth disabled. This cannot be checked explicitely because there is no SSH access.
		if config.HostPlatform == PlatformAKS {
			return true
		}
	}

	return false
}
