// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package addons

import (
	"os"

	cli "github.com/jawher/mow.cli"
	"github.com/olekukonko/tablewriter"
	"gitlab.com/commonground/haven/haven/cli/pkg/helm"
	"gitlab.com/commonground/haven/haven/cli/pkg/logging"
	"helm.sh/helm/v3/pkg/release"
)

func cmdList(cmd *cli.Cmd) {
	cmd.Action = func() {
		table := tablewriter.NewWriter(os.Stdout)
		table.SetAutoWrapText(false)
		table.SetHeader([]string{"Name", "Description", "Installed"})

		helmClient, err := helm.NewClient("")
		if err != nil {
			logging.Error("Error constructing client: %s\n", err)
			return
		}

		installedReleases, err := helmClient.List()

		addons, err := List()
		if err != nil {
			logging.Error("Error listing addons: %s\n", err)
			return
		}

		for _, addon := range addons {
			installedState := "NO"
			if isAddonInstalled(addon, installedReleases) {
				installedState = "YES"
			}

			table.Append([]string{addon.Name, addon.Description, installedState})
		}

		table.Render()
	}
}

func isAddonInstalled(addon Addon, installedReleases []*release.Release) bool {
NextRelease:
	for _, release := range addon.Releases {
		for _, installedRelease := range installedReleases {
			if release.Chart.Name == installedRelease.Chart.Name() && addon.Namespace == installedRelease.Namespace {
				continue NextRelease
			}
		}

		return false
	}

	return true
}
