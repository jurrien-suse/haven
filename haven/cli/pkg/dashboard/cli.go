// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

//go:generate pkger

package dashboard

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"os/exec"
	"runtime"

	"github.com/gorilla/mux"
	cli "github.com/jawher/mow.cli"
	"github.com/markbates/pkger"
	"gitlab.com/commonground/haven/haven/cli/pkg/connect"
	"gitlab.com/commonground/haven/haven/cli/pkg/logging"
	"k8s.io/kubectl/pkg/proxy"
)

const serveAddress = "127.0.0.1:3000"

// CLIConfig renders the command line function
var CLIConfig = func(config *cli.Cmd) {
	config.Action = func() {
		restConfig, err := connect.K8s()
		if err != nil {
			logging.Fatal("Error retrieving Kubernetes configuration: %s\n", err.Error())
		}

		filter := &proxy.FilterServer{
			AcceptPaths:   proxy.MakeRegexpArrayOrDie(proxy.DefaultPathAcceptRE),
			RejectPaths:   proxy.MakeRegexpArrayOrDie(proxy.DefaultPathRejectRE),
			AcceptHosts:   proxy.MakeRegexpArrayOrDie(proxy.DefaultHostAcceptRE),
			RejectMethods: proxy.MakeRegexpArrayOrDie(proxy.DefaultMethodRejectRE),
		}

		server, err := proxy.NewServer("", "/", "/static", filter, restConfig, 0)
		if err != nil {
			logging.Fatal("Error creating proxy: %s\n", err.Error())
		}

		go func() {
			listener, err := server.Listen("127.0.0.1", 3001)
			if err != nil {
				logging.Fatal("Could not listen to server: %s\n", err.Error())
			}

			server.ServeOnListener(listener)
		}()

		// no-op to include the whole static folder
		pkger.Include("/static")

		spaHandler := http.FileServer(&spaFileSystem{pkger.Dir("/static/haven-dashboard")})
		serveURL := fmt.Sprintf("http://%s", serveAddress)

		fmt.Printf("Opening browser on %s\n", serveURL)

		err = openBrowser(serveURL)
		if err != nil {
			logging.Fatal("Could not open browser: %s\n", err.Error())
		}

		remoteURL, _ := url.Parse("http://127.0.0.1:3001")
		proxyHandler := httputil.NewSingleHostReverseProxy(remoteURL)

		router := mux.NewRouter()
		router.PathPrefix("/apis").Handler(proxyHandler)
		router.PathPrefix("/api").Handler(proxyHandler)
		router.PathPrefix("/").Handler(spaHandler)

		err = http.ListenAndServe(serveAddress, router)
		if err != nil {
			logging.Fatal("Could not start dashboard: %s\n", err.Error())
		}
	}
}

func openBrowser(url string) error {
	switch runtime.GOOS {
	case "linux":
		return exec.Command("xdg-open", url).Start()
	case "windows":
		return exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		return exec.Command("open", url).Start()
	default:
		return fmt.Errorf("unsupported platform")
	}
}

type spaFileSystem struct {
	root http.FileSystem
}

func (fs *spaFileSystem) Open(name string) (http.File, error) {
	f, err := fs.root.Open(name)
	if os.IsNotExist(err) {
		return fs.root.Open("index.html")
	}
	return f, err
}
