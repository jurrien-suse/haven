# Open Policy Agent

Kubernetes by default has no policies protecting the cluster from harmful deployments. A user can mount a critical path on the host and extract valuable secrets that could lead to compromising the cluster. The presumption that containers make a system secure because they are isolated is incorrect, the cluster itself is still at a huge risk without proper policies.

## What can OPA gatekeeper do to secure the cluster?

OPA allows an administrator to define policies (rules and expectations that apply to deployments, pods, services etc.). These rules are validated at the Kubernetes API level, when a policy rule is triggered the request is denied and the user informed.

OPA policies are written in the "rego" language, this makes writing new policies very flexible with almost no dependencies compared to pod security policies that are bound to accounts.

## How does it work?

OPA gatekeeper is based on "constraint templates", this allows an administrator to add parameters to rego code. Templates can be reused with different parameters.

It is important to realize that constraint templates are static Kubernetes CRDs, they only define what is allowed and not. Constraints rules are used to apply these templates to the cluster.

For Haven we have developed a single constraint template that combines various checks that improve the security of the cluster.

- Services of type NodePort are not allowed
- Privileged containers are not allowed
- Pod resource limits are required and are restricted to 4000m and 8Gi

## Installation

To install OPA gatekeeper use:

```bash
./provision.sh
```

The installer will also ask you if you would like to install the default Haven policies.

## Validate the status

Validate the status of the installation.

```bash
./status.sh
```

## Destroy

Remove the configuration with:

```bash
./destroy.sh
```
