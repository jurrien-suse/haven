#!/bin/bash

# Copyright © VNG Realisatie 2019-2021
# Licensed under the EUPL

set -xe

kubectl -n kube-system get pods | grep dex
