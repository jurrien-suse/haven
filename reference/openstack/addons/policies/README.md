# Haven resource and network policies

By default a Kubernetes cluster has no resource or network policies. A pod that runs in the cluster is able to use all available resources and can connect to all other pods in the cluster, across namespaces. We provide Helm charts that allow you to easily configure resource and network policies. Notice that these policies are applied per namespace.
## Resource policies

Resource policies allow you to limit the cluster resources per namespace, think of the amount of cpu and memory, but also the amount of configmaps, secrets and storage. The chart will install a [LimitRange](./haven-quota-limitrange/templates/limitrange.yml) and several [ResourceQuotas](./haven-quota-limitrange/templates/quota.yml).

Note:

* Be default it is not permitted to create a NodePort or LoadBalancer service.
* There is a limit for the amount of configmaps, secrets, pods etc.
* A Limitrange function will apply basic resources for applications that have no resources spec defined.

In order to view the quotas that are in use run:

```bash
kubectl -n test describe quota

Name:                         storage
Namespace:                    test
Resource                      Used  Hard
--------                      ----  ----
count/persistentvolumeclaims  0     8
requests.storage              0     500Gi


Name:       object
Namespace:  test
Scopes:     NotTerminating
 * Matches all pods that do not have an active deadline. These pods usually include long running pods whose container command is not expected to terminate.
Resource                      Used  Hard
--------                      ----  ----
count/configmaps              0     16
count/cronjobs.batch          0     2
count/deployments.apps        0     8
count/deployments.extensions  0     8
count/jobs.batch              0     2
count/pods                    2     16
count/replicasets.apps        0     16
count/replicationcontrollers  0     8
count/resourcequotas          0     8
count/secrets                 0     16
count/services                0     8
count/services.loadbalancers  0     0
count/services.nodeports      0     0
count/statefulsets.apps       0     4


Name:       compute
Namespace:  test
Scopes:     NotTerminating
 * Matches all pods that do not have an active deadline. These pods usually include long running pods whose container command is not expected to terminate.
Resource       Used   Hard
--------       ----   ----
limits.cpu     750m   1500m
limits.memory  160Mi  2Gi
```

Take a close look at the "compute" quota, it describes the actual status of resource usage, in this output 50% cpu is in use.


## Network policies

Network policies rely heavy on labels, it's very important to label all relevant resources like pods and namespaces accordingly.

* Namespace isolation, only permit connections between pods in the same namespace.
* Permit ingress connections from the folowing namespaces: *monitoring* and *traefik*.
* Permit all egress traffic.

## Install

Install the default resource and network policies for a namespace with `addons/policies/provision.sh`. The script will ask for the namespace to apply the policies and install them.

## Upgrade

From Haven run `addons/policies/upgrade.sh`

## Remove

From Haven run `addons/policies/destroy.sh`.
