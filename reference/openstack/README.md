# Reference Implementation: Haven on OpenStack

## Kops
Haven can easily be deployed on OpenStack using [Kops](https://kops.sigs.k8s.io/). This is what we use internally at VNG Realisatie to work with Haven.

### How it works
The docker container wraps everything together to easily work with a Haven cluster on OpenStack.

When ready ensure your new cluster is Haven Compliant by using the [Haven Compliancy Checker](../../haven/cli/README.md).

#### Building the container
Prereq: place the [Haven CLI](../../haven/cli) binary next to the Dockerfile, because the docker context requires this to COPY it in.

Now run: `docker build -t registry.gitlab.com/commonground/haven/haven/reference_openstack .` to build the container.

#### Running the container
Follow the usage instructions in [./kops/README.md](./kops/README.md).

## Devstack
Development on this Reference Implementation can be assisted by deploying [Devstack](./devstack) resulting in a local OpenStack environment.

## Addons (legacy)
Please try not to use the [legacy addons](./addons) tied to this specific Reference Implementation, instead look into the [Haven CLI](../../haven/cli/README.md) for addons management.

## License
Copyright © VNG Realisatie 2019-2021
Licensed under the EUPL
